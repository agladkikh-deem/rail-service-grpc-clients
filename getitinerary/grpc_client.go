package getitinerary

import (
	"context"
	"fmt"
	railsearch "gitlab.com/deem-devops/phoenix/apis-go/rail/search-service/v1"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func GetItinerary(searchID, itineraryID string) {
	//serverURIqa10 := "172.16.37.41:31398"
	serverURILocal := "localhost:50053"
	conn, err := grpc.Dial(serverURILocal, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		panic(err)
	}

	clientSearch := railsearch.NewRailSearchServiceClient(conn)
	getItineraryReq := &railsearch.GetItineraryRequest{
		SearchId:    searchID,
		ItineraryId: itineraryID,
	}

	getItineraryResp, err := clientSearch.GetItinerary(context.Background(), getItineraryReq)
	if err != nil {
		panic(err)
	}

	fmt.Println("----------------------------------------")
	fmt.Println("get itinerary resp:")
	fmt.Printf("booking fee:\n%s\n", getItineraryResp.BookingFee)
	fmt.Printf("delivery fee:\n%s\n", getItineraryResp.DeliveryOption)
	//fmt.Printf("get itinerary:\n%s\n", getItineraryResp)

}
