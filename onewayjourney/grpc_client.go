package main

import (
	"context"
	"fmt"
	"math/rand"
	"rail-service-grpc-clients/getitinerary"
	"strings"

	v1 "gitlab.com/deem-devops/phoenix/apis-go/common/v1"
	customfields "gitlab.com/deem-devops/phoenix/apis-go/custom-fields/v1"
	railbooking "gitlab.com/deem-devops/phoenix/apis-go/rail/booking-service/v1"
	v12 "gitlab.com/deem-devops/phoenix/apis-go/rail/common/v1"
	railsearch "gitlab.com/deem-devops/phoenix/apis-go/rail/search-service/v1"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/metadata"
)

func main() {
	//serverURIqa10 := "172.16.37.41:31398"
	serverURILocal := "localhost:50053"
	conn, err := grpc.Dial(serverURILocal, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		panic(err)
	}

	clientSearch := railsearch.NewRailSearchServiceClient(conn)
	clientBooking := railbooking.NewRailReservationServiceClient(conn)
	clientRailCard := []*railsearch.RailCard{
		{
			GroupName:  "Popular Railcards",
			VendorName: "ATOC",
			VendorCode: "ATOC",
			Name:       "16-17 Saver",
			Code:       "op~U~183ad580-b0d9-4937-8c42-037da5ccedfe",
		},
	}
	createRailSearchReq := &railsearch.CreateRailSearchRequest{
		CustomerIdentifiers: &v1.CustomerIdentifiers{
			CustomerId: 3215491,
			SiteId:     245025,
			PartnerId:  10,
		},
		RouteType: railsearch.RouteType_ROUTE_TYPE_OPEN_RETURN,
		OutwardJourney: &railsearch.JourneyData{
			OriginLocation: &railsearch.JourneyData_OriginStation{
				//OriginStation: "urn:trainline:generic:loc:ECWK280gb",
				OriginStation: "urn:trainline:generic:loc:GLD5631gb",
			},
			DestinationLocation: &railsearch.JourneyData_DestinationStation{
				DestinationStation: "urn:trainline:generic:loc:WAT5598gb",
			},
			DateTime: &v1.DateTime{
				Year:  2024,
				Month: 9,
				Day:   1,
			},
			TimeType: railsearch.OutboundTimeType_OUTBOUND_TIME_TYPE_DEPARTS_AFTER,
		},
		RailCard: clientRailCard,
	}
	md := metadata.Pairs("deem-secctx", "{\"auth\":{\"ext\":{\"s\":245025}}}", "site-id", "245025")
	ctx := metadata.NewOutgoingContext(context.Background(), md)
	createRailSearchResp, err := clientSearch.CreateRailSearch(ctx, createRailSearchReq)

	if err != nil {
		panic(err)
	}

	searchID := createRailSearchResp.SearchId
	fmt.Println("----------------------------------------")
	fmt.Printf("SearchID=%s\n", searchID)

	getJourneyFaresReq := &railsearch.GetJourneyFaresRequest{
		SearchId: searchID,
	}

	getJourneyFaresResp, err := clientSearch.GetJourneyFares(ctx, getJourneyFaresReq)
	if err != nil {
		panic(err)
	}
	journeyIDFareID := getJourneyFaresResp.Journeys[0].Fares[0].Id
	journeyID := strings.Split(journeyIDFareID, "__")[1]
	fareID := strings.Split(journeyIDFareID, "__")[2]

	itineraryID := fmt.Sprintf("99%d", rand.Intn(9999999999))
	fmt.Println("----------------------------------------")
	fmt.Printf("ItineraryID=%s\n", itineraryID)
	updateItineraryReq := &railsearch.UpdateItineraryRequest{
		SearchId: searchID,
		OutwardJourney: &v12.JourneyFareIdentifier{
			JourneyId: journeyID,
			FareId:    fareID,
		},

		ItineraryId: itineraryID,
	}

	_, err = clientSearch.UpdateItinerary(ctx, updateItineraryReq)
	if err != nil {
		panic(err)
	}

	getJourneyPreferencesReq := &railsearch.GetJourneyPreferencesRequest{
		SearchId: searchID,
		OutwardJourney: &v12.JourneyFareIdentifier{
			JourneyId: journeyID,
			FareId:    fareID,
		},
	}
	_, err = clientSearch.GetJourneyPreferences(ctx, getJourneyPreferencesReq)
	if err != nil {
		panic(err)
	}

	updateItineraryReq = &railsearch.UpdateItineraryRequest{
		SearchId: searchID,
		OutwardJourney: &v12.JourneyFareIdentifier{
			JourneyId: journeyID,
			FareId:    fareID,
		},
		DeliveryOption: "AtocKiosk",
		//DeliveryOption: "AtocETicket",
		ItineraryId: itineraryID,
	}

	_, err = clientSearch.UpdateItinerary(ctx, updateItineraryReq)
	if err != nil {
		panic(err)
	}

	createReservationReq := &railbooking.CreateReservationRequest{
		SearchId:    searchID,
		ItineraryId: itineraryID,
		CreditCard: &v1.CreditCardToken{
			CreditCardUuid: "~LEGACY~clustera~153346",
			CreditCardType: v1.CreditCardType_CREDIT_CARD_TYPE_LEGACY,
		},
		CustomFields: []*customfields.CustomField{
			{
				Name:  "projectCode",
				Label: "projectCode",
				Value: "",
			},
			{
				Name:  "departmentCode",
				Label: "departmentCode",
				Value: "456",
			},
		},
	}

	createReservationResp, err := clientBooking.CreateReservation(ctx, createReservationReq)
	if err != nil {
		panic(err)
	}

	fmt.Println("----------------------------------------")
	fmt.Printf("create reservation resp:\n%s\n", createReservationResp.String())
	fmt.Println("------------------END----------------------")
	getitinerary.GetItinerary(searchID, itineraryID)
}
