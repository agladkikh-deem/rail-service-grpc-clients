package main

import (
	"context"
	"fmt"
	railsearch "gitlab.com/deem-devops/phoenix/apis-go/rail/search-service/v1"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"os"
)

func main() {
	//serverURIQA10 := "172.16.37.41:31398"
	serverURILocal := "localhost:50053"
	itineraryId := "3e913120-6a88-45aa-a0d2-79f0fdb0f7ff"

	req := &railsearch.GetETicketsRequest{
		ETicketType: railsearch.ETicketType_E_TICKET_TYPE_PDF,
		ItineraryId: itineraryId,
	}
	conn, err := grpc.Dial(serverURILocal, grpc.WithTransportCredentials(insecure.NewCredentials()))

	if err != nil {
		panic(err)
	}

	client := railsearch.NewRailSearchServiceClient(conn)

	resp, err := client.GetETickets(context.Background(), req)

	fmt.Println(err)

	if err == nil {
		for i, v := range resp.ETickets {
			err = os.WriteFile(fmt.Sprintf("./ticket%d.pdf", i), v, 0644)
			if err != nil {
				panic(err)
			}
		}
	}
}
